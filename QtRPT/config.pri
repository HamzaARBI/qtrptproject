CONFIG(debug, debug|release) {
    DEST_DIRECTORY = $$top_builddir/debug
}
CONFIG(release, debug|release) {
    DEST_DIRECTORY = $$top_builddir/release
}

#top_srcdir=$$PWD top_builddir=$$shadowed($$PWD)

DEFINES += QTRPT_LIBRARY    #Un-remark this line, if you want to build QtRPT as a library
#DEFINES += NO_BARCODE       #Un-remark this line, if you want to build QtRPT without BARCODE support
#DEFINES += QXLSX_LIBRARY    #Remark this line, if you want to build QtRPT without XLSX support
